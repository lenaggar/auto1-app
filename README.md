# AUTO1 app

Hi, This is my take on the JavaScript Engineer interview assignment for AUTO1.

To run the app just clone the repo, and `cd` into it, and then run the following command:

- If you use yarn: **`yarn && yarn start`**
- If you use npm: **`npm install && npm run start`**

  > Please make sure nothing is running on ports 3000 or 3001 on you machine's localhost

After the dependencies are downloaded and installed, The app should be up and running at [`http://localhost:3000/`](http://localhost:3000/)

I have used **Create-React-App** to create this app so that I won't get stuck too much on the configuration of the app. However, when I work on a long-term application, I would prefer creating my own configurations from scratch to have full control over the build process.

Please, don't hesitate to [contact me](mailto:naggar218@gmail.com) if you want to ask about anything.

Regards,  
_lenaggar_
