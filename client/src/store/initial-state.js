export default {
  entities: {
    manufacturers: {},
    models: {},
    cars: {}
  },
  ui: {
    cars: [],
    manufacturers: [],
    colors: [],
    totalPageCount: 0,
    loadingInitialData: true
  },

  // synced  with the client's local storage
  favorites: {}
};
