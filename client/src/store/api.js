import axios from "axios";

const api = axios.create({
  baseURL: "http://localhost:3001",
  common: { "X-Requested-With": "XMLHttpRequest" }
});

export default api;
