import { schema } from "normalizr";

export const color = new schema.Entity("colors");

export const model = new schema.Entity("models", {}, { idAttribute: "name" });

export const manufacturer = new schema.Entity(
  "manufacturers",
  { models: [model] },
  { idAttribute: "name" }
);

export const car = new schema.Entity(
  "cars",
  { color: color },
  { idAttribute: "stockNumber" }
);
