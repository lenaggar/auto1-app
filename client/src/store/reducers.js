import omit from "lodash/omit";

import {
  FETCH_COLORS,
  FETCH_MANUFACTURERS,
  FETCH_CARS,
  FETCH_CAR,
  ADD_CAR_TO_FAVS,
  REMOVE_CAR_FROM_FAVS,
  INITIAL_DATA_LOADED
} from "./actions";

export default (state, action) => {
  switch (action.type) {
    case `${FETCH_COLORS}_FULFILLED`:
      return {
        ...state,
        ui: {
          ...state.ui,
          colors: action.payload.colors
        }
      };

    case `${FETCH_MANUFACTURERS}_FULFILLED`:
      return {
        ...state,
        entities: {
          ...state.entities,
          models: action.payload.manufacturers.entities.models,
          manufacturers: action.payload.manufacturers.entities.manufacturers
        },
        ui: {
          ...state.ui,
          manufacturers: action.payload.manufacturers.result
        }
      };

    case `${FETCH_CARS}_FULFILLED`:
      return {
        ...state,
        entities: {
          ...state.entities,
          cars: {
            ...state.entities.cars,
            ...action.payload.cars.entities.cars
          }
        },
        ui: {
          ...state.ui,
          cars: action.payload.cars.result,
          totalPageCount: action.payload.totalPageCount
        }
      };

    case `${FETCH_CAR}_FULFILLED`:
      return {
        ...state,
        entities: {
          ...state.entities,
          cars: {
            ...state.entities.cars,
            ...action.payload.cars.entities.cars
          }
        }
      };

    case ADD_CAR_TO_FAVS:
      return {
        ...state,
        favorites: {
          ...state.favorites,
          [action.payload]: action.payload
        }
      };

    case REMOVE_CAR_FROM_FAVS:
      return {
        ...state,
        favorites: omit(state.favorites, action.payload)
      };

    case INITIAL_DATA_LOADED:
      return {
        ...state,
        ui: {
          ...state.ui,
          loadingInitialData: false
        }
      };

    default:
      return state;
  }
};
