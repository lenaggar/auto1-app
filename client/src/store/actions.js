import { normalize } from "normalizr";
import qs from "qs";
import api from "./api";
import {
  car as carSchema,
  manufacturer as manufacturerSchema
} from "./schemas";

export const FETCH_COLORS = "FETCH_COLORS";
export const fetchColors = () => ({
  type: FETCH_COLORS,
  payload: api.get("/colors").then(res => res.data)
});

export const FETCH_MANUFACTURERS = "FETCH_MANUFACTURERS";
export const fetchManufacturers = () => ({
  type: FETCH_MANUFACTURERS,
  payload: api
    .get("/manufacturers")
    .then(res => res.data)
    .then(({ manufacturers }) => ({
      manufacturers: normalize(manufacturers, [manufacturerSchema])
    }))
});

export const FETCH_CARS = "FETCH_CARS";
export const fetchCars = params => dispatch => {
  const qString = qs.stringify(params);
  const url = qString ? `/cars?${qString}` : "/cars";

  return dispatch({
    type: FETCH_CARS,
    payload: api
      .get(url)
      .then(res => res.data)
      .then(({ cars, totalPageCount }) => ({
        cars: normalize(cars, [carSchema]),
        totalPageCount
      }))
  });
};

export const FETCH_CAR = "FETCH_CAR";
export const fetchCar = stockNumber => ({
  type: FETCH_CAR,
  payload: api
    .get(`/cars/${stockNumber}`)
    .then(res => res.data)
    .then(({ car }) => ({ cars: normalize([car], [carSchema]) }))
});

export const INITIAL_DATA_LOADED = "INITIAL_DATA_LOADED";
export const doneLoadingInitialData = () => ({ type: INITIAL_DATA_LOADED });

export const ADD_CAR_TO_FAVS = "ADD_CAR_TO_FAVS";
export const addToFavs = stockNumber => ({
  type: ADD_CAR_TO_FAVS,
  payload: stockNumber
});

export const REMOVE_CAR_FROM_FAVS = "REMOVE_CAR_FROM_FAVS";
export const removeFromFavs = stockNumber => ({
  type: REMOVE_CAR_FROM_FAVS,
  payload: stockNumber
});
