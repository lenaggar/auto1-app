const makeCarUIObject = car => ({
  stockNumber: car.stockNumber,
  pictureUrl: car.pictureUrl,
  title: `${car.manufacturerName} ${car.modelName}`,
  subtitle: `Stock # ${car.stockNumber} - ${
    car.mileage.number
  } ${car.mileage.unit.toUpperCase()} - ${car.fuelType} - ${car.color}`
});

export const selectCar = state => stockNumber => {
  const { entities, favorites } = state;

  const car = entities.cars[stockNumber];

  return car
    ? {
        ...makeCarUIObject(entities.cars[stockNumber]),
        isFavorite: !!favorites[stockNumber]
      }
    : {};
};

export const selectCars = state => {
  const { entities, ui, favorites } = state;

  const cars = ui.cars;

  return cars.length > 0
    ? cars.map(id => ({
        ...makeCarUIObject(entities.cars[id]),
        isFavorite: !!favorites[id]
      }))
    : [];
};

export const selectColorsList = state => state.ui.colors;
export const getColorsDropdownOptions = state => {
  const colorsDropDownOptions = selectColorsList(state).map(color => ({
    value: color,
    label: color
  }));

  return [{ value: "all", label: "All car colors" }].concat(
    colorsDropDownOptions
  );
};

export const selectManufacturersList = state => state.ui.manufacturers;
export const getManufacturersDropdownOptions = state => {
  const manDropDownOptions = selectManufacturersList(state).map(man => ({
    value: man,
    label: man
  }));

  return [{ value: "all", label: "All manufacturers" }].concat(
    manDropDownOptions
  );
};

export const selectNumbers = state => ({
  currentlyShowing: state.ui.cars.length,
  totalPageCount: state.ui.totalPageCount
});

export const getInitialLoadState = state => state.ui.loadingInitialData;
