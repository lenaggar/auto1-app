import { createStore, compose, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import promiseMiddleware from "redux-promise-middleware";
import { createLogger } from "redux-logger";
import persistState from "redux-localstorage";

import rootReducer from "./reducers";
import initialState from "./initial-state";

const middleware = [
  thunk,
  promiseMiddleware(),
  createLogger({ collapsed: true })
];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const storeEnhancer = composeEnhancers(
  applyMiddleware(...middleware),
  persistState("favorites")
);

export default function configureStore() {
  return createStore(rootReducer, initialState, storeEnhancer);
}
