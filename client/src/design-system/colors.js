export const L100 = "#EDEDED";
export const D100 = "#4A4A4A";
export const O100 = "#EA7F28";
export const O100_SEMI_TRANS = "#FFEA7F28";
export const O200 = "#D37324";
export const _WHITE = "#FFF";
