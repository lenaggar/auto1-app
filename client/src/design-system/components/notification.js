import styled from "styled-components";

import { O100_SEMI_TRANS, O200 } from "../colors";

export default styled.div({
  margin: "0 0 12px",
  padding: "8px",

  border: `1px solid ${O200}`,
  borderRadius: "5px",

  backgroundColor: O100_SEMI_TRANS,
  color: O200
});
