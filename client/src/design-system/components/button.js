import styled from "styled-components";

import { O100, O200, _WHITE } from "../colors";
import { Paragraph1Styles } from "../typography";

export default styled.button(props => ({
  ...Paragraph1Styles,
  width: "128px",
  height: "32px",
  margin: "8px 0 0 0",

  border: 0,
  borderRadius: "2px",

  backgroundColor: O100,

  color: _WHITE,

  alignSelf: props.right && "flex-end",

  "&:hover": {
    backgroundColor: O200
  }
}));
