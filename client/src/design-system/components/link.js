import { Link } from "react-router-dom";
import styled from "styled-components";

import { O100, O200 } from "../colors";
import { Paragraph1Styles } from "../typography";

export default styled(Link)(({ css = {} }) => ({
  ...Paragraph1Styles,
  color: O100,
  textDecoration: "none",
  cursor: "pointer",
  ...css,

  "&:hover": {
    color: O200,
    textDecoration: "underline"
  }
}));
