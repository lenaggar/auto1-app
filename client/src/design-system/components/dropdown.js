import React, { Component } from "react";
import Dropdown from "react-dropdown";
import styled from "styled-components";

import "react-dropdown/style.css";

import { L100, O100, _WHITE } from "../colors";
import { Paragraph1Styles, Paragraph2Styles } from "../typography";

const StyledDropdown = styled(Dropdown)({
  "&.is-open .Dropdown-arrow": {
    borderColor: `transparent transparent ${L100}`,
    borderWidth: "0 6px 6px"
  },

  "& .Dropdown-arrow": {
    borderColor: `${L100} transparent transparent`,
    borderWidth: "6px 6px 0"
  },

  "& .Dropdown-control": {
    ...Paragraph1Styles,
    margin: "0",
    padding: "8px",

    border: `1.5px solid ${L100}`,
    borderRadius: "3px"
  },

  "& .Dropdown-menu": {
    marginTop: "8px",

    border: `1.5px solid ${L100}`,
    borderRadius: "3px",

    "& .Dropdown-option": {
      ...Paragraph1Styles,
      margin: "0",
      padding: "8px",

      "&.is-selected": {
        backgroundColor: _WHITE
      },

      "&:not(:last-child)": {
        borderBottom: `1.5px solid ${L100}`
      },

      "&:hover": {
        color: _WHITE,
        backgroundColor: O100
      }
    }
  }
});

const Label = styled.label({
  ...Paragraph2Styles,

  display: "inline-block",
  marginBottom: "8px"
});

const Wrapper = styled.div({
  marginBottom: "12px"
});

class DropdownComponent extends Component {
  render() {
    const { id, label, ...rest } = this.props;

    return (
      <Wrapper>
        <Label htmlFor={id}>{label}</Label>

        <StyledDropdown id={id} {...rest} />
      </Wrapper>
    );
  }
}

export default DropdownComponent;
