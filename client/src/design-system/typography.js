import styled from "styled-components";
import { D100 } from "./colors";

const _base = {
  margin: "0 0 12px 0",
  fontFamily: '"Roboto", sans-serif',
  color: D100
};

export const TitleStyles = {
  ..._base,
  fontSize: "32px",
  fontWeight: "bold"
};
export const Title = styled.h1(TitleStyles);

export const HeadingStyles = {
  ..._base,
  fontSize: "18px",
  fontWeight: "bold"
};
export const Heading = styled.h3(HeadingStyles);

export const SubtitleStyles = {
  ..._base,
  fontSize: "18px",
  fontWeight: "normal"
};
export const Subtitle = styled.p(SubtitleStyles);

export const Paragraph1Styles = {
  ..._base,
  fontSize: "14px",
  fontWeight: "normal"
};
export const Paragraph1 = styled.p(Paragraph1Styles);

export const Paragraph2Styles = {
  ..._base,
  fontSize: "12px",
  fontWeight: "normal"
};
export const Paragraph2 = styled.p(Paragraph2Styles);
