import React from "react";

import Header from "./screens/shared/header";
import Footer from "./screens/shared/footer";
import Routes from "./routes";

const App = () => (
  <>
    <Header />
    <Routes />
    <Footer />
  </>
);

export default App;
