import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import HomePage from "./screens/home-page";
import ListCars from "./screens/list-cars";
import Car from "./screens/car";
import NotFound from "./screens/not-found";

const Routes = () => (
  <Switch>
    <Route exact path="/" component={HomePage} />
    <Route exact path="/cars" component={ListCars} />
    <Route path="/cars/:stockNumber" component={Car} />
    <Route path="/not-found" component={NotFound} />

    {/* fall-back for any other unlisted url */}
    <Redirect to="/not-found" />
  </Switch>
);

export default Routes;
