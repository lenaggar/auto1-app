import React from "react";
import styled from "styled-components";

import CompanyLogo from "./shared/company-logo";
import Link from "../design-system/components/link";
import { Title, Subtitle } from "../design-system/typography";

const Wrapper = styled.main({
  minHeight: "calc(100vh - 160px)",
  margin: "80px auto 0",

  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",

  lineHeight: "1.7"
});

const NotFound = () => (
  <Wrapper>
    <CompanyLogo />

    <br />

    <Title>404 - Not Found</Title>

    <Subtitle>Sorry, the page you are looking for does not exist.</Subtitle>

    <Subtitle>
      {"You can always go back to the "}
      <Link to="/" css={{ fontSize: "18px" }}>
        homepage
      </Link>
      .
    </Subtitle>
  </Wrapper>
);

export default NotFound;
