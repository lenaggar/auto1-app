import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

import CompanyLogo from "./company-logo";
import MyStyledLink from "../../design-system/components/link";
import { L100, D100, _WHITE } from "../../design-system/colors";

const Wrapper = styled.header({
  position: "fixed",
  top: "0",
  right: "0",
  left: "0",
  zIndex: "100",
  backgroundColor: _WHITE,
  borderBottom: `1px solid ${L100}`
});

const MaxWidthWrapper = styled.div({
  height: "80px",
  maxWidth: "1080px",
  margin: "auto",
  padding: "0 12px 0 24px",

  display: "flex",
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center"
});

const HeaderNav = styled.nav({
  alignSelf: "stretch",

  "& ul": {
    margin: 0,
    padding: 0,
    height: "100%",

    display: "flex",
    flexDirection: "row"
  }
});

const NavLink = styled(MyStyledLink)({
  padding: "0 12px",
  display: "inline-block",
  lineHeight: "80px",
  color: D100
});

const Header = () => (
  <Wrapper>
    <MaxWidthWrapper>
      <Link to="/">
        <CompanyLogo />
      </Link>

      <HeaderNav>
        <ul>
          <li>
            <NavLink to="#">Purchase</NavLink>
          </li>
          <li>
            <NavLink to="#">My Orders</NavLink>
          </li>
          <li>
            <NavLink to="#">Sell</NavLink>
          </li>
        </ul>
      </HeaderNav>
    </MaxWidthWrapper>
  </Wrapper>
);

export default Header;
