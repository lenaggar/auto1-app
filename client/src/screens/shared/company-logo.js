import React from "react";
import styled from "styled-components";

const LogoComp = styled.img({
  height: "32px"
});

const CompanyLogo = () => <LogoComp src="logo.png" alt="company logo" />;

export default CompanyLogo;
