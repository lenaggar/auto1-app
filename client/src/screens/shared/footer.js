import React from "react";
import styled from "styled-components";

import { L100, _WHITE } from "../../design-system/colors";

const Wrapper = styled.footer({
  height: "80px",
  padding: "0 12px 0 24px",

  backgroundColor: _WHITE,
  borderTop: `1px solid ${L100}`,

  display: "flex",
  justifyContent: "center",
  alignItems: "center"
});

const Footer = () => (
  <Wrapper>
    <span>&copy; AUTO1 Group 2018</span>
  </Wrapper>
);

export default Footer;
