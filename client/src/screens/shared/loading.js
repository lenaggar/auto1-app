// import PropTypes from "prop-types";
import styled, { keyframes } from "styled-components";
import { D100, _WHITE } from "../../design-system/colors";

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

const Spinner = styled.div`
  margin: auto;
  animation: ${rotate} 0.8s linear infinite;
  box-sizing: border-box;
  border-radius: 50%;
  border: 0.4rem solid ${D100};
  width: ${props => props.size || "32px"};
  height: ${props => props.size || "32px"};
  border-width: ${props => props.thickness || "4px"};
  border-color: ${props => props.ringColor || D100};
  border-top-color: ${props => props.spinnerColor || _WHITE};
  display: ${props => (props.inline ? "inline-block" : "block")};
`;

export default Spinner;
