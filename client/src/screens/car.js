import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import styled from "styled-components";

import { fetchCar, addToFavs, removeFromFavs } from "../store/actions";
import { selectCar } from "../store/selectors";

import Loading from "./shared/loading";
import Button from "../design-system/components/button";
import { L100 } from "../design-system/colors";
import { Title, Subtitle, Paragraph1 } from "../design-system/typography";

const Wrapper = styled.main(props => ({
  minHeight: "calc(100vh - 160px)",
  maxWidth: props.wide ? "none" : "1080px",
  margin: "80px auto 0",
  padding: !props.wide && "32px 24px",

  display: "flex",
  flexDirection: "column",

  lineHeight: "1.7"
}));

const CarImage = styled.div({
  minHeight: "360px",
  backgroundColor: L100,
  marginBottom: "24px"
});

const Details = styled.div({
  maxWidth: "800px",
  margin: "0 auto",

  display: "flex",
  flexDirection: "row"
});

const CarData = styled.div({
  flex: "1"
});

const AddToFavs = styled.div({
  flex: "0 0 40%",
  alignSelf: "flex-start",

  margin: "0 0 0 40px",
  padding: "24px",
  border: `1px solid ${L100}`,

  display: "flex",
  flexDirection: "column"
});

class Car extends Component {
  state = { loading: true, validStockNumber: false };

  componentDidMount() {
    const { isInvalidNumber, carStockNumber, fetchCar } = this.props;

    if (isInvalidNumber) {
      this.setState({ loading: false, validStockNumber: false });
      return;
    }

    // also here I am causing a delay of a 1 second
    // to simulate network request and show loading state
    fetchCar(carStockNumber)
      .then(() =>
        setTimeout(
          () => this.setState({ loading: false, validStockNumber: true }),
          1000
        )
      )
      .catch(() =>
        setTimeout(
          () => this.setState({ loading: false, validStockNumber: false }),
          1000
        )
      );
  }

  render() {
    const {
      state: { loading, validStockNumber },
      props: { car, addToFavs, removeFromFavs }
    } = this;

    if (loading) {
      return (
        <Wrapper>
          <Loading />
        </Wrapper>
      );
    }

    if (!validStockNumber) {
      return <Redirect to="/wrong-url" />;
    }

    return (
      <Wrapper wide>
        <CarImage />

        <Details>
          <CarData>
            <Title>{car.title}</Title>
            <Subtitle>{car.subtitle}</Subtitle>

            <Paragraph1>
              This car is currently available and can be delivered as soon as
              tomorrow morning. Please be aware that delivery times shown in
              this page are not definitive and may change due to bad weather
              conditions.
            </Paragraph1>
          </CarData>

          <AddToFavs>
            <Paragraph1>
              If you like this car, click the button and save it in your
              collection of favourite items.
            </Paragraph1>

            {car.isFavorite ? (
              <Button right onClick={removeFromFavs}>
                Unsave
              </Button>
            ) : (
              <Button right onClick={addToFavs}>
                Save
              </Button>
            )}
          </AddToFavs>
        </Details>
      </Wrapper>
    );
  }
}

const mapState = state => ({ selectCar: selectCar(state) });

const mapDispatch = { fetchCar, addToFavs, removeFromFavs };

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const carStockNumber = ownProps.match.params.stockNumber;
  const isInvalidNumber = isNaN(+carStockNumber);

  return {
    car: stateProps.selectCar(carStockNumber),

    fetchCar: dispatchProps.fetchCar,
    addToFavs: () => dispatchProps.addToFavs(carStockNumber),
    removeFromFavs: () => dispatchProps.removeFromFavs(carStockNumber),

    carStockNumber,
    isInvalidNumber
  };
};

export default connect(
  mapState,
  mapDispatch,
  mergeProps
)(Car);
