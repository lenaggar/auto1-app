import React from "react";
import styled from "styled-components";

import { L100 } from "../../design-system/colors";
import { Heading, Paragraph1 } from "../../design-system/typography";

const Wrapper = styled.li({
  position: "relative",
  padding: "16px",
  "&:not(:last-child)": {
    marginBottom: "12px"
  },

  border: `1px solid ${L100}`,

  display: "flex",
  flexDirection: "row"
});

const dimmedStyled = {
  color: L100,
  backgroundColor: L100
};

const EmptyCar = styled.div({
  height: "80px",
  width: "95px",
  marginRight: "24px",
  ...dimmedStyled
});

const DimmedCarTitle = styled(Heading)(dimmedStyled);

const DimmedDetails = styled(Paragraph1)(dimmedStyled);

const DimmedLink = styled.span(dimmedStyled);

const CarItemWithLoadingState = () => (
  <Wrapper>
    <EmptyCar />

    <div>
      <DimmedCarTitle>Mercedes-Benz Vito Tourer</DimmedCarTitle>

      <DimmedDetails>Stock # 29839 - 107.613 KM - Petrol - Red</DimmedDetails>

      <DimmedLink>View details</DimmedLink>
    </div>
  </Wrapper>
);

export default CarItemWithLoadingState;
