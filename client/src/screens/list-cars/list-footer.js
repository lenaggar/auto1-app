import React, { Component } from "react";
import styled from "styled-components";

import Link from "../../design-system/components/link";

const Wrapper = styled.footer({
  display: "flex",
  justifyContent: "center",

  "& > *": {
    margin: "0",

    "&:not(:last-child)": {
      margin: "0 28px 0 0"
    }
  }
});

class ListFooter extends Component {
  render() {
    const {
      props: { page, totalPageCount, firstUrl, prevUrl, nextUrl, lastUrl }
    } = this;

    return (
      <Wrapper>
        <Link
          css={{ visibility: +page > 1 ? "visible" : "hidden" }}
          to={firstUrl}
        >
          First
        </Link>

        <Link
          css={{
            visibility: +page >= 2 ? "visible" : "hidden"
          }}
          to={prevUrl}
        >
          Previous
        </Link>

        <span>
          Page {page} of {totalPageCount}
        </span>

        <Link
          css={{
            visibility: +page + 1 <= totalPageCount ? "visible" : "hidden"
          }}
          to={nextUrl}
        >
          Next
        </Link>

        <Link
          css={{
            visibility: +page < totalPageCount ? "visible" : "hidden"
          }}
          to={lastUrl}
        >
          Last
        </Link>
      </Wrapper>
    );
  }
}

export default ListFooter;
