import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";

import {
  fetchColors,
  fetchManufacturers,
  doneLoadingInitialData
} from "../../store/actions";
import { getInitialLoadState } from "../../store/selectors";

import Loading from "../shared/loading";
import SideFilters from "./side-filters";
import MainContent from "./main-content";

const Wrapper = styled.main({
  minHeight: "calc(100vh - 160px)",
  maxWidth: "1080px",
  margin: "80px auto 0",
  padding: "32px 24px",

  display: "flex",
  flexDirection: "row"
});

class Main extends Component {
  componentDidMount() {
    const {
      fetchManufacturers,
      fetchColors,
      doneLoadingInitialData
    } = this.props;

    // delay of a 1 second to simulate network request and loading state
    Promise.all([fetchManufacturers(), fetchColors()]).then(() =>
      setTimeout(doneLoadingInitialData, 1000)
    );
  }

  render() {
    return (
      <Wrapper>
        {this.props.loadingInitialData ? (
          <Loading />
        ) : (
          <>
            <SideFilters />
            <MainContent />
          </>
        )}
      </Wrapper>
    );
  }
}

const mapState = state => ({ loadingInitialData: getInitialLoadState(state) });

const mapDispatch = { fetchColors, fetchManufacturers, doneLoadingInitialData };

export default withRouter(
  connect(
    mapState,
    mapDispatch
  )(Main)
);
