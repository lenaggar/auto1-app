import React from "react";
import styled from "styled-components";

import Link from "../../design-system/components/link";
import { L100, O100 } from "../../design-system/colors";
import {
  Heading,
  HeadingStyles,
  Paragraph1
} from "../../design-system/typography";

const Wrapper = styled.li({
  position: "relative",
  padding: "16px",
  "&:not(:last-child)": {
    marginBottom: "12px"
  },

  border: `1px solid ${L100}`,

  display: "flex",
  flexDirection: "row"
});

const CarImage = styled.img({
  height: "80px",
  width: "95px",
  marginRight: "24px"
});

const Details = styled(Paragraph1)({
  textTransform: "capitalize"
});

const Fav = styled.span({
  ...HeadingStyles,
  color: O100,
  position: "absolute",
  top: "16px",
  right: "16px"
});

const CarItem = ({ stockNumber, pictureUrl, title, subtitle, isFavorite }) => (
  <Wrapper>
    <CarImage src={pictureUrl} alt="car" />

    <div>
      <Heading>{title}</Heading>
      <Details>{subtitle}</Details>
      <Link to={`/cars/${stockNumber}`}>View details</Link>
    </div>

    {isFavorite && <Fav>&#9733;</Fav>}
  </Wrapper>
);

export default CarItem;
