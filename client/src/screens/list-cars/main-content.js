import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";
import qs from "qs";
import omit from "lodash/omit";

import { fetchCars } from "../../store/actions";
import { selectCars, selectNumbers } from "../../store/selectors";

import CarsList from "./cars-list";
import CarsListWithLoadingState from "./cars-list-with-loading-state";
import ListFooter from "./list-footer";
import Dropdown from "../../design-system/components/dropdown";
import Link from "../../design-system/components/link";
import { Heading, Subtitle } from "../../design-system/typography";

const Wrapper = styled.section({
  marginLeft: "24px",
  flex: "1"
});

const ListHeader = styled.header({
  marginBottom: "12px",

  display: "flex",
  flexDirection: "row",
  justifyContent: "Space-between",

  "& :last-child": {
    flex: "0 0 280px"
  }
});

const sortList = ["asc", "desc"];

const sortDropdownOptions = [
  { value: "none", label: "None" },
  { value: "asc", label: "Mileage - Ascending" },
  { value: "desc", label: "Mileage - Descending" }
];

class MainContent extends Component {
  state = { loading: true };

  componentDidMount() {
    this.props
      .fetchCars()
      // delay of half a second to simulate loading
      .then(() => setTimeout(() => this.setState({ loading: false }), 500));
  }

  componentDidUpdate(prevProps) {
    if (prevProps.location.search !== this.props.location.search) {
      this.setState({ loading: true }, () => {
        this.props
          .fetchCars()
          // delay of half a second to simulate loading
          .then(() => setTimeout(() => this.setState({ loading: false }), 500));
      });
    }
  }

  handleSortChange = ({ value: newSortOrder }) => {
    const {
      validateSort,
      location: { search, pathname },
      history
    } = this.props;

    const currentQSObj = qs.parse(search.substr(1));

    if (newSortOrder === "none" && !currentQSObj.sort) return;

    // do nothing if same 'sort' is selected
    if (newSortOrder === currentQSObj.sort) return;

    const newQS = qs.stringify({
      // remove 'sort' and 'manufacturer' from current url search obj
      ...omit(currentQSObj, "sort"),

      // add 'sort' only if it's a valid sort
      ...(validateSort(newSortOrder) && { sort: newSortOrder })
    });

    const url = newQS ? `${pathname}?${newQS}` : pathname;

    console.log(url);

    history.push(url);
  };

  render() {
    const {
      state: { loading },
      props: {
        cars,
        stats,
        sort,
        page,
        navigationUrls,
        isValidPage,
        invalidPageGoBack
      },
      handleSortChange
    } = this;

    if (!isValidPage) {
      return (
        <Wrapper>
          <Subtitle>
            This is an invalid page number or maybe out of range.
          </Subtitle>

          <Link to={invalidPageGoBack} css={{ fontSize: "18px" }}>
            go back!
          </Link>
        </Wrapper>
      );
    }

    return (
      <Wrapper>
        <ListHeader>
          <div>
            <Heading>Available Cars</Heading>
            <Subtitle>Showing {stats.currentlyShowing} results</Subtitle>
          </div>

          <Dropdown
            id="sort-by"
            label="Sort by"
            options={sortDropdownOptions}
            value={sort || "none"}
            onChange={handleSortChange}
          />
        </ListHeader>

        {loading ? (
          <CarsListWithLoadingState cars={[...new Array(cars.length)]} />
        ) : (
          <CarsList cars={cars} />
        )}

        <ListFooter
          page={page}
          totalPageCount={stats.totalPageCount}
          {...navigationUrls}
        />
      </Wrapper>
    );
  }
}

const mapState = state => ({
  cars: selectCars(state),
  stats: selectNumbers(state)
});

const mapDispatch = { fetchCars };

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    location: { search, pathname }
  } = ownProps;

  const currentQSObj = qs.parse(search.substr(1));
  const { sort, page = 1 } = currentQSObj;

  const validateSort = sortOpt =>
    sortOpt !== "none" && sortList.includes(sortOpt);

  const validatePage = page =>
    !isNaN(page) && page > 0 && page <= stateProps.stats.totalPageCount;

  return {
    ...stateProps,
    ...ownProps,

    fetchCars: () => dispatchProps.fetchCars(currentQSObj),

    sort: validateSort(sort) && sort,
    validateSort,
    isValidSort: sort ? validateSort(sort) : true,

    page: validatePage(page) && page,
    validatePage,
    isValidPage: validatePage(page),

    invalidPageGoBack: validatePage(page)
      ? "#"
      : `${pathname}?${qs.stringify(omit(currentQSObj, "page"))}`,

    navigationUrls: {
      firstUrl: validatePage(page)
        ? `${pathname}?${qs.stringify({ ...currentQSObj, page: 1 })}`
        : "#",
      prevUrl: validatePage(page)
        ? `${pathname}?${qs.stringify({
            ...currentQSObj,
            page: +page - 1
          })}`
        : "#",
      nextUrl: validatePage(page)
        ? `${pathname}?${qs.stringify({
            ...currentQSObj,
            page: +page + 1
          })}`
        : "#",
      lastUrl: validatePage(page)
        ? `${pathname}?${qs.stringify({
            ...currentQSObj,
            page: stateProps.stats.totalPageCount
          })}`
        : "#"
    }
  };
};

export default withRouter(
  connect(
    mapState,
    mapDispatch,
    mergeProps
  )(MainContent)
);
