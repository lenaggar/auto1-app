import React, { PureComponent } from "react";
import styled from "styled-components";

import CarItem from "./car-item";

const CarsContainer = styled.ul({
  margin: "0 0 32px",
  padding: "0"
});

class CarsList extends PureComponent {
  render() {
    const { cars } = this.props;

    return (
      <CarsContainer>
        {cars.map(car => (
          <CarItem key={car.stockNumber} {...car} />
        ))}
      </CarsContainer>
    );
  }
}

export default CarsList;
