import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import qs from "qs";
import omit from "lodash/omit";
import styled from "styled-components";

import {
  selectColorsList,
  getColorsDropdownOptions,
  selectManufacturersList,
  getManufacturersDropdownOptions
} from "../../store/selectors";

import { L100 } from "../../design-system/colors";
import Dropdown from "../../design-system/components/dropdown";
import Button from "../../design-system/components/button";
import Notification from "../../design-system/components/notification";

const Wrapper = styled.aside({
  padding: "24px",

  alignSelf: "flex-start",
  flex: "0 0 30%",

  border: `1px solid ${L100}`,

  display: "flex",
  flexDirection: "column"
});

class SideFilters extends Component {
  state = {
    color: this.props.color,
    manufacturer: this.props.manufacturer
  };

  handleColorChange = ({ value }) => {
    this.setState({ color: value });
  };

  handleManChange = ({ value }) => {
    this.setState({ manufacturer: value });
  };

  handleFilterClick = () => {
    const {
      state: { color, manufacturer },
      props: {
        validateColor,
        validateManufacturer,
        location: { search, pathname },
        history
      }
    } = this;

    const currentQSObj = qs.parse(search.substr(1));

    const newQS = qs.stringify({
      // remove 'color', 'manufacturer', 'page' from current url search obj
      ...omit(currentQSObj, ["page", "color", "manufacturer"]),

      // add 'color' only if it's a valid color
      ...(validateColor(color) && { color }),

      // add 'manufacturer' only if it's a valid manufacturer
      ...(validateManufacturer(manufacturer) && { manufacturer })
    });

    const url = newQS ? `${pathname}?${newQS}` : pathname;

    history.push(url);
  };

  render() {
    const {
      state: { color, manufacturer },
      props: {
        colorsDropdownOptions,
        isValidColor,
        manufacturersDropdownOptions,
        isValidManufacturer
      },
      handleColorChange,
      handleManChange,
      handleFilterClick
    } = this;

    return (
      <Wrapper>
        {!isValidColor && (
          <Notification>
            Note: the selected color in the url is not a valid color or there
            are no cars with this color.
            <br />
            You can use the valid filters below and look through our available
            options
          </Notification>
        )}

        {!isValidManufacturer && (
          <Notification>
            Note: the selected manufacturer in the url is not a valid
            manufacturer or there are no cars from this manufacturer.
            <br />
            You can use the valid filters below and look through our available
            options
          </Notification>
        )}

        <Dropdown
          id="color-filter"
          label="Color"
          options={colorsDropdownOptions}
          value={color || "all"}
          onChange={handleColorChange}
        />

        <Dropdown
          id="manufacturer-filter"
          label="Manufacturer"
          options={manufacturersDropdownOptions}
          value={manufacturer || "all"}
          onChange={handleManChange}
        />

        <Button right onClick={handleFilterClick}>
          Filter
        </Button>
      </Wrapper>
    );
  }
}

const mapState = state => ({
  colorsList: selectColorsList(state),
  colorsDropdownOptions: getColorsDropdownOptions(state),
  manufacturersList: selectManufacturersList(state),
  manufacturersDropdownOptions: getManufacturersDropdownOptions(state)
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    location: { search }
  } = ownProps;

  const { color, manufacturer } = qs.parse(search.substr(1));

  const validateColor = color =>
    color !== "all" && stateProps.colorsList.includes(color);

  const validateManufacturer = manufacturer =>
    manufacturer !== "all" &&
    stateProps.manufacturersList.includes(manufacturer);

  return {
    ...ownProps,

    colorsDropdownOptions: stateProps.colorsDropdownOptions,
    manufacturersDropdownOptions: stateProps.manufacturersDropdownOptions,
    loadingInitialData: stateProps.loadingInitialData,

    color: validateColor(color) && color,
    validateColor,
    isValidColor: color ? validateColor(color) : true,

    manufacturer: validateManufacturer(manufacturer) && manufacturer,
    validateManufacturer,
    isValidManufacturer: manufacturer
      ? validateManufacturer(manufacturer)
      : true
  };
};

export default withRouter(
  connect(
    mapState,
    null,
    mergeProps
  )(SideFilters)
);
