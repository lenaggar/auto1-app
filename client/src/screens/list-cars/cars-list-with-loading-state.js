import React, { PureComponent } from "react";
import styled from "styled-components";

import CarItemWithLoadingState from "./car-item-with-loading-state";

const CarsContainer = styled.ul({
  margin: "0 0 32px",
  padding: "0"
});

class CarsListWithLoadingState extends PureComponent {
  render() {
    const { cars } = this.props;

    return (
      <CarsContainer>
        {cars.map((car, i) => (
          <CarItemWithLoadingState key={i} />
        ))}
      </CarsContainer>
    );
  }
}

export default CarsListWithLoadingState;
