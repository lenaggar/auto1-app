import React from "react";
import styled from "styled-components";

import CompanyLogo from "./shared/company-logo";
import Link from "../design-system/components/link";
import { Title } from "../design-system/typography";

const Wrapper = styled.main({
  minHeight: "calc(100vh - 160px)",
  margin: "80px auto 0",

  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",

  lineHeight: "1.7"
});

const HomePage = () => (
  <Wrapper>
    <CompanyLogo />

    <br />

    <Title>Welcome to AUTO1 simple app</Title>

    <Link to="/cars" css={{ fontSize: "18px" }}>
      Go to our used cars listing app >>>
    </Link>
  </Wrapper>
);

export default HomePage;
